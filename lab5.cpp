/*
 Name: Tripp Tucker
 Username: rftucke
 Assignment: Lab 5
 Lab Section: 2
 TA: Hollis Liu
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

//Each card has two important values: its suit and its number value.
typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  
  //random seed generator; will provide true randomness every time the code is ran.
  srand(unsigned (time(0)));

  //Initializing the card deck array
  //There are 52 cards in a deck: 13 values * 4 different suits
  Card cards[52];
  int i = 0;
  int j = 0;
  int k = 0;
  //The nested for loop will generate 4 suits of cards that have 13 possible values (Ace - King)
  for (i = 2; i <= 14; i++) {
    for (j = 0; j <= 3; j++) {
      if (j == 0) {
	cards[k].suit = SPADES;
      }
      else if (j == 1) {
	cards[k].suit = HEARTS;
      }
      else if (j == 2) {
	cards[k].suit = DIAMONDS;
      }
      else {
	cards[k].suit = CLUBS;
      }
	  
      cards[k].value = i;
      //Iterates to the next card
      k = k+1; 
    }
  }
   

  //random_shuffle will shuffle the card deck array that was just created.
  random_shuffle(&cards[0], &cards[52], *myrandom);

  /*An array of size 5 is created to hold the first five
   randomized cards after random_shuffle changes the order
   of the 52-slot array */
  Card deck[5];
  //Copying the first 5 randomized cards to the new array
  for (i = 0; i < 5; i++) {
    deck[i] = cards[i];
  }

  /*calling sort will allow the program to order the cards by suit
    if the suit is the same, the cards will be ordered by value */
  
  sort(&deck[0], &deck[5], suit_order);

  /*Once the cards have been sorted by suit and value, this for loop
    will print out the 5 ordered cards */
  for (i = 0; i < 5; i++) {
    cout << setw(10) << right << get_card_name(deck[i]) << " of " << get_suit_code(deck[i]) << endl;
  }
  
  return 0;
}


/*This function determines the order of cards by looking at
  the cards suit and then the card value if the right hand 
  suit and left hand suit match */
bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit < rhs.suit){
    return true;
  }
  else if (lhs.suit == rhs.suit) {
    if (lhs.value < rhs.value) {
      return true;
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
}

// This function takes the suit value (0 - 3) and returns the name to the actual suit
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

// This function takes the card value (2 - 14) and returns the value to the actual name of the card.
string get_card_name(Card& c) {
  switch(c.value) {
  case 2: return "2";
  case 3: return "3";
  case 4: return "4";
  case 5: return "5";
  case 6: return "6";
  case 7: return "7";
  case 8: return "8";
  case 9: return "9";
  case 10: return "10";
  case 11: return "Jack";
  case 12: return "Queen";
  case 13: return "King";
  case 14: return "Ace";
  default: return "";
  }
}
